package com.javaboy.vhr.service.impl;

import com.javaboy.vhr.entity.JobLevel;
import com.javaboy.vhr.mapper.JobLevelMapper;
import com.javaboy.vhr.service.JobLevelService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-01-21 15:09:25
 * @description: 职称(JobLevel)表服务实现类
 */
@Service("jobLevelService")
public class JobLevelServiceImpl implements JobLevelService {
    @Resource
    private JobLevelMapper jobLevelMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public JobLevel queryById(Integer id) {
        return this.jobLevelMapper.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<JobLevel> queryAllByLimit(int offset, int limit) {
        return this.jobLevelMapper.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param jobLevel 实例对象
     * @return 实例对象
     */
    @Override
    public JobLevel insert(JobLevel jobLevel) {
        jobLevel.setCreateDate(new Date());
        jobLevel.setEnabled(true);
        this.jobLevelMapper.insert(jobLevel);
        return jobLevel;
    }

    /**
     * 修改数据
     *
     * @param jobLevel 实例对象
     * @return 实例对象
     */
    @Override
    public JobLevel update(JobLevel jobLevel) {
        this.jobLevelMapper.update(jobLevel);
        return this.queryById(jobLevel.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.jobLevelMapper.deleteById(id) > 0;
    }

    @Override
    public List<JobLevel> getAllJobLevels() {
        return this.jobLevelMapper.getAllJobLevels();
    }

    @Override
    public void delJobLevesByIds(Integer[] ids) {
        this.jobLevelMapper.delJobLevesByIds(ids);
    }

    @Override
    public List<JobLevel> queryAll(JobLevel jobLevel) {
        return this.jobLevelMapper.queryAll(jobLevel);
    }
}