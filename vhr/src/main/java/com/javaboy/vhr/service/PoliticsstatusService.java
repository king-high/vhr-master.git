package com.javaboy.vhr.service;

import com.javaboy.vhr.entity.Politicsstatus;

import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-02-04 10:13:06
 * @description: 政治面貌(Politicsstatus)表服务接口
 */
public interface PoliticsstatusService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Politicsstatus queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Politicsstatus> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param politicsstatus 实例对象
     * @return 实例对象
     */
    Politicsstatus insert(Politicsstatus politicsstatus);

    /**
     * 修改数据
     *
     * @param politicsstatus 实例对象
     * @return 实例对象
     */
    Politicsstatus update(Politicsstatus politicsstatus);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    /**
     * 通过实体作为筛选条件查询
     * @param politicsstatus
     * @return
     */
    List<Politicsstatus> queryAll(Politicsstatus politicsstatus);
}