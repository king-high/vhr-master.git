package com.javaboy.vhr.service.impl;

import com.javaboy.vhr.entity.Nation;
import com.javaboy.vhr.mapper.NationMapper;
import com.javaboy.vhr.service.NationService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-02-04 10:12:44
 * @description: 民族(Nation)表服务实现类
 */
@Service("nationService")
public class NationServiceImpl implements NationService {
    @Resource
    private NationMapper nationMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Nation queryById(Integer id) {
        return this.nationMapper.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Nation> queryAllByLimit(int offset, int limit) {
        return this.nationMapper.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param nation 实例对象
     * @return 实例对象
     */
    @Override
    public Nation insert(Nation nation) {
        this.nationMapper.insert(nation);
        return nation;
    }

    /**
     * 修改数据
     *
     * @param nation 实例对象
     * @return 实例对象
     */
    @Override
    public Nation update(Nation nation) {
        this.nationMapper.update(nation);
        return this.queryById(nation.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.nationMapper.deleteById(id) > 0;
    }

    /**
     * 通过实体作为筛选条件查询
     * @param nation
     * @return
     */
    @Override
    public List<Nation> queryAll(Nation nation) {
        return this.nationMapper.queryAll(nation);
    }
}