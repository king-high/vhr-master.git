package com.javaboy.vhr.service;

import com.github.pagehelper.PageInfo;
import com.javaboy.vhr.entity.Employee;

import java.util.Date;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-02-02 14:22:31
 * @description: 雇员(Employee)表服务接口
 */
public interface EmployeeService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Employee queryById(Integer id);

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Employee> queryAllByLimit(int offset, int limit);

    /**
     * 新增数据
     *
     * @param employee 实例对象
     * @return 实例对象
     */
    Employee insert(Employee employee);

    /**
     * 修改数据
     *
     * @param employee 实例对象
     * @return 实例对象
     */
    Employee update(Employee employee);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);

    /**
     * 分页条件查询
     * @param employee
     * @param pageNo
     * @param pageSize
     * @param beginDateScope
     * @return
     */
    PageInfo<Employee> getEmployeePage(Employee employee, Integer pageNo, Integer pageSize, Date[] beginDateScope);

    /**
     * 查询最大工号
     * @return
     */
    Integer maxWorkID();

}