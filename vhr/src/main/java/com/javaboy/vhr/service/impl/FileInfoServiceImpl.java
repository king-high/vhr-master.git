package com.javaboy.vhr.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javaboy.vhr.entity.FileInfo;
import com.javaboy.vhr.mapper.FileInfoMapper;
import com.javaboy.vhr.service.FileInfoService;
import com.javaboy.vhr.utils.UuidUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-03-23 10:46:35
 * @description: 文件(FileInfo)表服务实现类
 */
@Service("fileInfoService")
public class FileInfoServiceImpl implements FileInfoService {
    @Resource
    private FileInfoMapper fileInfoMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public FileInfo queryById(String id) {
        return this.fileInfoMapper.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<FileInfo> queryAllByLimit(int offset, int limit) {
        return this.fileInfoMapper.queryAllByLimit(offset, limit);
    }

    /**
     * 分页查询
     *
     * @param fileInfo
     * @param pageNo
     * @param pageSize
     * @return
     */
    @Override
    public PageInfo<FileInfo> pageList(FileInfo fileInfo, Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo, pageSize);
        List<FileInfo> fileInfoList = this.fileInfoMapper.queryAll(fileInfo);
        PageInfo<FileInfo> pageInfo = new PageInfo<>(fileInfoList);
        return pageInfo;
    }

    /**
     * 新增数据
     *
     * @param fileInfo 实例对象
     * @return 实例对象
     */
    @Override
    public FileInfo insert(FileInfo fileInfo) {
        fileInfo.setId(UuidUtil.getShortUuid());
        fileInfo.setCreatedAt(new Date());
        fileInfo.setUpdatedAt(new Date());
        this.fileInfoMapper.insert(fileInfo);
        return fileInfo;
    }

    /**
     * 修改数据
     *
     * @param fileInfo 实例对象
     * @return 实例对象
     */
    @Override
    public FileInfo update(FileInfo fileInfo) {
        fileInfo.setUpdatedAt(new Date());
        this.fileInfoMapper.update(fileInfo);
        return this.queryById(fileInfo.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(String id) {
        return this.fileInfoMapper.deleteById(id) > 0;
    }

    /**
     * 通过文件标识查询
     * @param fileKey
     * @return
     */
    @Override
    public FileInfo queryByKey(String fileKey) {
        return this.fileInfoMapper.queryByKey(fileKey);
    }
}