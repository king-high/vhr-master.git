package com.javaboy.vhr.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.javaboy.vhr.entity.Employee;
import com.javaboy.vhr.mapper.EmployeeMapper;
import com.javaboy.vhr.service.EmployeeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-02-02 14:22:31
 * @description: 雇员(Employee)表服务实现类
 */
@Service("employeeService")
public class EmployeeServiceImpl implements EmployeeService {
    @Resource
    private EmployeeMapper employeeMapper;

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public Employee queryById(Integer id) {
        return this.employeeMapper.queryById(id);
    }

    /**
     * 查询多条数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    @Override
    public List<Employee> queryAllByLimit(int offset, int limit) {
        return this.employeeMapper.queryAllByLimit(offset, limit);
    }

    /**
     * 新增数据
     *
     * @param employee 实例对象
     * @return 实例对象
     */
    @Override
    public Employee insert(Employee employee) {
        this.employeeMapper.insert(employee);
        return employee;
    }

    /**
     * 修改数据
     *
     * @param employee 实例对象
     * @return 实例对象
     */
    @Override
    public Employee update(Employee employee) {
        this.employeeMapper.update(employee);
        return this.queryById(employee.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.employeeMapper.deleteById(id) > 0;
    }

    @Override
    public PageInfo<Employee> getEmployeePage(Employee employee, Integer pageNo, Integer pageSize, Date[] beginDateScope) {
        PageHelper.startPage(pageNo,pageSize);
        List<Employee> list = this.employeeMapper.queryAll(employee);
        return new PageInfo<>(list);
    }

    @Override
    public Integer maxWorkID() {
        return this.employeeMapper.maxWorkID();
    }
}