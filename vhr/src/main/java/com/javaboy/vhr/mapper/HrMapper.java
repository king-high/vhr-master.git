package com.javaboy.vhr.mapper;

import com.javaboy.vhr.entity.Hr;
import com.javaboy.vhr.entity.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * (Hr)表数据库访问层
 *
 * @author makejava
 * @since 2020-10-24 14:25:33
 */
@Mapper
public interface HrMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    Hr queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<Hr> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param hr 实例对象
     * @return 对象列表
     */
    List<Hr> queryAll(Hr hr);

    /**
     * 新增数据
     *
     * @param hr 实例对象
     * @return 影响行数
     */
    int insert(Hr hr);

    /**
     * 修改数据
     *
     * @param hr 实例对象
     * @return 影响行数
     */
    int update(Hr hr);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

    /**
     * 根据用户名加载用户对象
     * @param username
     * @return
     */
    Hr loadUserByUsername(String username);

    /**
     * 通过id获取角色信息
     * @param id
     * @return
     */
    List<Role> getHrRolesById(Integer id);

    /**
     * 获取所有HR
     * @param hrId
     * @param keywords
     * @return
     */
    List<Hr> getAllHrs(@Param("hrId") Integer hrId, @Param("keywords") String keywords);
}