package com.javaboy.vhr.mapper;

import com.javaboy.vhr.entity.CertFile;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-02-25 16:51:55
 * @description: 证书文件(CertFile)表数据库访问层
 */
public interface CertFileMapper {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    CertFile queryById(Integer id);

    /**
     * 查询指定行数据
     *
     * @param offset 查询起始位置
     * @param limit  查询条数
     * @return 对象列表
     */
    List<CertFile> queryAllByLimit(@Param("offset") int offset, @Param("limit") int limit);


    /**
     * 通过实体作为筛选条件查询
     *
     * @param certFile 实例对象
     * @return 对象列表
     */
    List<CertFile> queryAll(CertFile certFile);

    /**
     * 新增数据
     *
     * @param certFile 实例对象
     * @return 影响行数
     */
    int insert(CertFile certFile);

    /**
     * 批量新增数据（MyBatis原生foreach方法）
     *
     * @param entities List<CertFile> 实例对象列表
     * @return 影响行数
     */
    int insertBatch(@Param("entities") List<CertFile> entities);

    /**
     * 批量新增或按主键更新数据（MyBatis原生foreach方法）
     *
     * @param entities List<CertFile> 实例对象列表
     * @return 影响行数
     */
    int insertOrUpdateBatch(@Param("entities") List<CertFile> entities);

    /**
     * 修改数据
     *
     * @param certFile 实例对象
     * @return 影响行数
     */
    int update(CertFile certFile);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 影响行数
     */
    int deleteById(Integer id);

}