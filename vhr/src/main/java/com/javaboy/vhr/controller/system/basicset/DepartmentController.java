package com.javaboy.vhr.controller.system.basicset;

import com.javaboy.vhr.entity.Department;
import com.javaboy.vhr.service.DepartmentService;
import com.javaboy.vhr.utils.result.ResultDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-01-09 14:48
 * @description: 部门管理Api
 */
@Slf4j
@Api(tags = "部门管理Api")
@RestController
@RequestMapping("/system/basic/department")
public class DepartmentController {

    @Resource
    private DepartmentService departmentService;

    @ApiOperation(value = "获取所有部门信息")
    @GetMapping("/list")
    public ResultDTO<List<Department>> getAllDepartments(){
        List<Department> list = this.departmentService.getAllDepartments();
        return ResultDTO.success(list);
    }

    @ApiOperation(value = "添加部门")
    @PostMapping("/add")
    public ResultDTO<Department> addDepartment(@RequestBody Department department){
        Department insert = this.departmentService.insert(department);
        if(log.isInfoEnabled()){
            log.info("{} instance {} was created.", Department.class.getSimpleName(), insert.getId());
        }
        return ResultDTO.success("添加成功",insert);
    }

    @ApiOperation(value = "添加部门")
    @PostMapping("/edit")
    public ResultDTO<Department> editeDepartment(@RequestBody Department department){
        Department update = this.departmentService.update(department);
        if(log.isInfoEnabled()){
            log.info("{} instance {} was updated.", Department.class.getSimpleName(), update.getId());
        }
        return ResultDTO.success("修改成功",update);
    }

    @ApiOperation(value = "删除部门")
    @DeleteMapping("/delete")
    public ResultDTO<Void> delDepartment(@RequestParam(value = "id", required = true) Integer id){
        this.departmentService.deleteById(id);
        if(log.isInfoEnabled()){
            log.info("{} instance {} was deleted.", Department.class.getSimpleName(), id);
        }
        return ResultDTO.success("删除成功");
    }

}
