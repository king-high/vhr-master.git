package com.javaboy.vhr.controller.system;

import com.javaboy.vhr.entity.Hr;
import com.javaboy.vhr.entity.Role;
import com.javaboy.vhr.service.HrService;
import com.github.pagehelper.PageInfo;
import com.javaboy.vhr.service.RoleService;
import com.javaboy.vhr.utils.result.ResultDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-02-24 18:12:38
 * @description: (Hr)表控制层
 */
@Api(tags = "")
@RestController
@RequestMapping("/system/hr")
public class HrController {

    @Resource
    private HrService hrService;
    @Resource
    private RoleService roleService;

    @ApiOperation(value = "查询所有hr")
    @GetMapping("/list")
    public ResultDTO<List<Hr>> getAllHrs(String keywords) {
        List<Hr> hrs = hrService.getAllHrs(keywords);
        return ResultDTO.success(hrs);
    }

    @ApiOperation(value = "查询所有角色")
    @GetMapping("/roles")
    public List<Role> getAllRoles() {
        return roleService.getAllRoles();
    }

    @ApiOperation(value = "新增")
    @PostMapping("/add")
    public ResultDTO<Hr> add(@RequestBody Hr hr) {
        Hr model = this.hrService.insert(hr);
        return ResultDTO.success(model);
    }

    @ApiOperation(value = "修改")
    @PutMapping("/edit")
    public ResultDTO<Hr> edit(@RequestBody Hr hr) {
        Hr model = this.hrService.update(hr);
        return ResultDTO.success(model);
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("/delete/{id}")
    public ResultDTO<Void> delete(@PathVariable Integer id) {
        this.hrService.deleteById(id);
        return ResultDTO.success("删除成功");
    }

}