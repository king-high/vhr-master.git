package com.javaboy.vhr.controller;

import com.javaboy.vhr.entity.CertFile;
import com.javaboy.vhr.service.CertFileService;
import com.javaboy.vhr.utils.result.ResultDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.*;
import java.net.URLEncoder;

/**
 * @author: gaoyang
 * @date: 2021-02-25 16:51:52
 * @description: 证书文件(CertFile)表控制层
 */
@Slf4j
@Api(tags = "证书文件API")
@RestController
@RequestMapping("/certFile")
public class CertFileController {

    @Resource
    private CertFileService certFileService;

    @ApiOperation(value = "上传文件")
    @PostMapping("/import")
    public ResultDTO<CertFile> add(@RequestParam(name = "file", required = true) MultipartFile file) throws IOException {
        byte[] bytes = file.getBytes();
        CertFile certFile = new CertFile();
        certFile.setFile(bytes);
        certFile.setFileName(file.getOriginalFilename());
        CertFile model = this.certFileService.insert(certFile);
        return ResultDTO.success(model);
    }

    @ApiOperation(value = "下载")
    @GetMapping("/download")
    public ResponseEntity<InputStreamResource> download(@RequestParam(name = "id", required = true) Integer id) throws IOException {
        CertFile certFile = this.certFileService.queryById(id);
        File file = new File(certFile.getFileName());
        FileOutputStream fos = new FileOutputStream(file);
        fos.write(certFile.getFile(), 0, certFile.getFile().length);
        fos.flush();
        fos.close();

        if(certFile!=null){
            // 文件的原始名称

            InputStream inputStream = new FileInputStream(file);
            InputStreamResource inputStreamResource = new InputStreamResource(inputStream);
            HttpHeaders headers = new HttpHeaders();

            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            String encode = URLEncoder.encode(certFile.getFileName(), "UTF-8");
            headers.add("Content-Disposition", String.format("attachment; filename=\"%s\"", encode));
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentLength(file.length())
                    .contentType(MediaType.parseMediaType("application/octet-stream"))
                    .body(inputStreamResource);
        }else {
            InputStream inputStream = new ByteArrayInputStream("<script language=\"javascript\">alert('文件不存在！');</script>".getBytes("GBK"));
            InputStreamResource inputStreamResource = new InputStreamResource(inputStream);
            HttpHeaders headers = new HttpHeaders();
            headers.add(HttpHeaders.CONTENT_TYPE,"text/html;charset=UTF-8");
            headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
            headers.add("Pragma", "no-cache");
            headers.add("Expires", "0");
            return ResponseEntity
                    .ok()
                    .headers(headers)
                    .contentType(MediaType.TEXT_HTML)
                    .body(inputStreamResource);
        }

    }

}