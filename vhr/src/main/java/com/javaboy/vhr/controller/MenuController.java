package com.javaboy.vhr.controller;

import com.javaboy.vhr.entity.Menu;
import com.javaboy.vhr.service.MenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2020-12-24 15:29:34
 * @description: 菜单(Menu)表控制层
 */
@Api(tags = "菜单API")
@RestController
@RequestMapping("/menu")
public class MenuController {

    @Resource
    private MenuService menuService;

    @ApiOperation(value = "通过主键查询单条数据")
    @GetMapping("/selectOne")
    public Menu selectOne(Integer id) {
        return this.menuService.queryById(id);
    }


    @ApiOperation(value = "查询权限树形菜单")
    @GetMapping("/selectTreeMenu")
    public List<Menu> queryAll(){
        List<Menu> list = this.menuService.queryAll();
        return list;
    }


}