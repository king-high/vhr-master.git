package com.javaboy.vhr.controller.system.basicset;

import com.javaboy.vhr.entity.JobLevel;
import com.javaboy.vhr.entity.Position;
import com.javaboy.vhr.service.PositionService;
import com.javaboy.vhr.utils.result.ResultDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-01-09 14:55:57
 * @description: 职位表(Position)表控制层
 */
@Slf4j
@Api(tags = "职位API")
@RestController
@RequestMapping("/system/basic/pos")
public class PositionController {

    @Resource
    private PositionService positionService;

    @ApiOperation(value = "获取所有职位信息")
    @GetMapping("/list")
    public ResultDTO<List<Position>> getAllPosition() {
        List<Position> positionList = this.positionService.getAllPosition();
        return ResultDTO.success(positionList);
    }

    @ApiOperation(value = "新增职位")
    @PostMapping("/add")
    public ResultDTO<Void> addPosition(@RequestBody Position position) {
        Position insert = this.positionService.insert(position);
        if (log.isInfoEnabled()) {
            log.info("{} instance {} was created.", Position.class.getSimpleName(), insert.getId());
        }
        return ResultDTO.success("新增成功");
    }

    @ApiOperation(value = "修改职位")
    @PutMapping("/edit")
    public ResultDTO<Position> editPosition(@RequestBody Position position) {
        Position update = this.positionService.update(position);
        if (log.isInfoEnabled()) {
            log.info("{} instance {} was updated.", Position.class.getSimpleName(), update.getId());
        }
        return ResultDTO.success("修改成功", update);
    }

    @ApiOperation(value = "删除职位")
    @DeleteMapping("/delete")
    public ResultDTO<Void> deletePositionById(@RequestParam(value = "id", required = true)Integer id) {
        this.positionService.deleteById(id);
        if (log.isInfoEnabled()) {
            log.info("{} instance {} was deleted.", Position.class.getSimpleName(), id);
        }
        return ResultDTO.success("删除成功");
    }

    @ApiOperation(value = "批量删除职位")
    @DeleteMapping("/deleteList")
    public ResultDTO<Void> deletePositionsByIds(@RequestParam(value = "ids", required = true) Integer[] ids) {
        this.positionService.deletePositionsByIds(ids);
        if(log.isInfoEnabled()){
            log.info("{} instance {} was deleted.", JobLevel.class.getSimpleName(), ids);
        }
        return ResultDTO.success("删除成功");
    }

}