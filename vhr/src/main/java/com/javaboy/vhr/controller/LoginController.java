package com.javaboy.vhr.controller;

import com.javaboy.vhr.utils.VerificationCode;
import com.javaboy.vhr.utils.result.ResultDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * @Author: gaoyang
 * @Date: 2020/10/24  15:05
 * @Description:
 */
@Api(tags = "未登录API")
@RestController
public class LoginController {

    @ApiOperation(value = "未登录返回提示信息")
    @GetMapping("/login")
    public ResultDTO hello(){
        return ResultDTO.error("尚未登录，请登录");
    }

    @ApiOperation(value = "生成验证码图片")
    @GetMapping("/verifyCode")
    public void verifyCode(HttpServletRequest request, HttpServletResponse resp) throws IOException {
        VerificationCode code = new VerificationCode();
        BufferedImage image = code.getImage();
        String text = code.getText();
        HttpSession session = request.getSession(true);
        session.setAttribute("verify_code", text);
        VerificationCode.output(image,resp.getOutputStream());
    }

}
