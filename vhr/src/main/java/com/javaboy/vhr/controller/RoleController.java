package com.javaboy.vhr.controller;

import com.javaboy.vhr.entity.Role;
import com.javaboy.vhr.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author: gaoyang
 * @date: 2020-12-24 13:28:16
 * @description: 角色(Role)表控制层
 */
@Api(tags = "角色API")
@RestController
@RequestMapping("/role")
public class RoleController {

    @Resource
    private RoleService roleService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @ApiOperation(value = "通过主键查询单条数据")
    @GetMapping("/selectOne")
    public Role selectOne(Integer id) {
        return this.roleService.queryById(id);
    }

}