package com.javaboy.vhr.controller.employee;

import com.github.pagehelper.PageInfo;
import com.javaboy.vhr.entity.*;
import com.javaboy.vhr.service.*;
import com.javaboy.vhr.utils.result.ResultDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-02-02 14:22:29
 * @description: 雇员(Employee)表控制层
 */
@Slf4j
@Api(tags = "雇员API")
@RestController
@RequestMapping("/employee/basic")
public class EmployeeController {

    @Resource
    private EmployeeService employeeService;
    @Resource
    private NationService nationService;
    @Resource
    private PoliticsstatusService politicsstatusService;
    @Resource
    private JobLevelService jobLevelService;
    @Resource
    private PositionService positionService;
    @Resource
    private DepartmentService departmentService;


    @ApiOperation(value = "雇员分页查询")
    @GetMapping("/page")
    public ResultDTO<PageInfo<Employee>> getEmployeePage(@RequestParam(name = "pageNo", defaultValue = "1") Integer pageNo,
                                                         @RequestParam(name = "pageSize", defaultValue = "10") Integer pageSize,
                                                         Employee employee, Date[] beginDateScope) {
        PageInfo<Employee> pageInfo = this.employeeService.getEmployeePage(employee, pageNo, pageSize, beginDateScope);
        return ResultDTO.success(pageInfo);
    }

    @ApiOperation(value = "添加雇员")
    @PostMapping("/add")
    public ResultDTO<Employee> addEmp(@RequestBody Employee employee) {
        Employee insert = this.employeeService.insert(employee);
        if (log.isInfoEnabled()) {
            log.info("{} instance {} was updated.", Employee.class.getSimpleName(), insert.getId());
        }
        return ResultDTO.success("添加成功", insert);
    }

    @ApiOperation(value = "根据id删除雇员")
    @DeleteMapping("/{id}")
    public ResultDTO<Void> deleteEmpByEid(@PathVariable Integer id) {
        boolean b = this.employeeService.deleteById(id);
        if (b) {
            return ResultDTO.success("删除成功");
        }
        return ResultDTO.error("删除失败！");
    }

    @ApiOperation(value = "修改雇员")
    @PutMapping("/edit")
    public ResultDTO<Employee> updateEmp(@RequestBody Employee employee) {
        Employee update = this.employeeService.update(employee);
        if (log.isInfoEnabled()) {
            log.info("{} instance {} was updated.", Employee.class.getSimpleName(), update.getId());
        }
        return ResultDTO.success("修改成功", update);
    }

    @ApiOperation(value = "查询民族列表")
    @GetMapping("/nations")
    public ResultDTO<List<Nation>> getAllNations(Nation nation) {
        List<Nation> list = this.nationService.queryAll(nation);
        return ResultDTO.success(list);
    }

    @ApiOperation(value = "政治面貌列表")
    @GetMapping("/politicsstatus")
    public ResultDTO<List<Politicsstatus>> getAllPoliticsstatus(Politicsstatus politicsstatus) {
        List<Politicsstatus> list = this.politicsstatusService.queryAll(politicsstatus);
        return ResultDTO.success(list);
    }

    @ApiOperation(value = "查询职称列表")
    @GetMapping("/joblevels")
    public ResultDTO<List<JobLevel>> getAllJobLevels(JobLevel jobLevel) {
        List<JobLevel> list = this.jobLevelService.queryAll(jobLevel);
        return ResultDTO.success(list);
    }

    @ApiOperation(value = "查询职位列表")
    @GetMapping("/positions")
    public ResultDTO<List<Position>> getAllPositions(Position position) {
        List<Position> list = this.positionService.queryAll(position);
        return ResultDTO.success(list);
    }

    @GetMapping("/maxWorkID")
    public ResultDTO<String> maxWorkId() {
        String str = String.format("%08d", employeeService.maxWorkID() + 1);
        return ResultDTO.success("success",str);
    }

    @ApiOperation(value = "查询部门列表")
    @GetMapping("/deps")
    public ResultDTO<List<Department>> getAllDepartments() {
        List<Department> list = departmentService.getAllDepartments();
        return ResultDTO.success(list);
    }

}