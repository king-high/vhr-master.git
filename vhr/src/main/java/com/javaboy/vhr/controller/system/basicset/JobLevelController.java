package com.javaboy.vhr.controller.system.basicset;

import com.javaboy.vhr.entity.JobLevel;
import com.javaboy.vhr.service.JobLevelService;
import com.javaboy.vhr.utils.result.ResultDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-01-21 15:09:23
 * @description: 职称(JobLevel)表控制层
 */
@Slf4j
@Api(tags = "职称API")
@RestController
@RequestMapping("/system/basic/joblevel")
public class JobLevelController {

    @Resource
    private JobLevelService jobLevelService;

    @ApiOperation(value = "查询所有职称信息")
    @GetMapping("/list")
    public ResultDTO<List<JobLevel>> getAllJobLevels() {
        List<JobLevel> list = this.jobLevelService.getAllJobLevels();
        return ResultDTO.success(list);
    }

    @ApiOperation(value = "添加职称")
    @PostMapping("/add")
    public ResultDTO<JobLevel> addJobLevel(@RequestBody JobLevel jobLevel) {
        JobLevel insert = this.jobLevelService.insert(jobLevel);
        if (log.isInfoEnabled()) {
            log.info("{} instance {} was created.", JobLevel.class.getSimpleName(), insert.getId());
        }
        return ResultDTO.success("添加成功", insert);
    }

    @ApiOperation(value = "修改职称")
    @PutMapping("/edit")
    public ResultDTO<JobLevel> editJobLevel(@RequestBody JobLevel jobLevel) {
        JobLevel update = this.jobLevelService.update(jobLevel);
        if (log.isInfoEnabled()) {
            log.info("{} instance {} was created.", JobLevel.class.getSimpleName(), update.getId());
        }
        return ResultDTO.success("修改成功", update);
    }

    @ApiOperation(value = "删除职称")
    @DeleteMapping("/delete")
    public ResultDTO<Void> delJobLeve(@RequestParam(value = "id", required = true) Integer id) {
        this.jobLevelService.deleteById(id);
        if (log.isInfoEnabled()) {
            log.info("{} instance {} was deleted.", JobLevel.class.getSimpleName(), id);
        }
        return ResultDTO.success("删除成功");
    }

    @ApiOperation(value = "批量删除职称")
    @DeleteMapping("/deleteList")
    public ResultDTO<Void> delJobLevesByIds(@RequestParam(value = "ids", required = true) Integer[] ids) {
        this.jobLevelService.delJobLevesByIds(ids);
        if (log.isInfoEnabled()) {
            log.info("{} instance {} was deleted.", JobLevel.class.getSimpleName(), ids);
        }
        return ResultDTO.success("删除成功");
    }

}