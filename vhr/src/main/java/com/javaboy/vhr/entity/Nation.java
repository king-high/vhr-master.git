package com.javaboy.vhr.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author: gaoyang
 * @date: 2021-02-03 17:44:45
 * @description: 民族(Nation)实体类
 */
@Getter
@Setter
@ApiModel("民族实体类")
public class Nation implements Serializable {
    private static final long serialVersionUID = 367820871893887312L;

    @ApiModelProperty("自增主键")
    private Integer id;

    @ApiModelProperty("民族名称")
    private String name;

}