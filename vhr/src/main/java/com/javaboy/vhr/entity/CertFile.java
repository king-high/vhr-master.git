package com.javaboy.vhr.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author: gaoyang
 * @date: 2021-02-25 16:51:16
 * @description: 证书文件(CertFile)实体类
 */
@Getter
@Setter
@ApiModel("证书文件实体类")
public class CertFile implements Serializable {
    private static final long serialVersionUID = 574300215327371771L;

    @ApiModelProperty("自增主键")
    private Integer id;

    @ApiModelProperty("证书文件名")
    private String fileName;

    @ApiModelProperty("证书文件")
    private byte[] file;

}