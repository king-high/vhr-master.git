package com.javaboy.vhr.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author: gaoyang
 * @date: 2021-01-20 14:13:59
 * @description: 部门(Department)实体类
 */
@Getter
@Setter
@ApiModel("部门实体类")
public class Department implements Serializable {
    private static final long serialVersionUID = 368006487666358558L;

    @ApiModelProperty("主键")
    private Integer id;

    @ApiModelProperty("部门名称")
    private String name;

    @ApiModelProperty("父级id")
    private Integer parentId;

    @ApiModelProperty("")
    private String depPath;

    @ApiModelProperty("是否启用")
    private Boolean enabled;

    @ApiModelProperty("是否父级菜单")
    private Boolean isParent;

    private List<Department> children = new ArrayList<>();

}