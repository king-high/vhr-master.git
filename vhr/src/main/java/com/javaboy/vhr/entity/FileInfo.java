package com.javaboy.vhr.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author: gaoyang
 * @date: 2021-03-23 10:46:31
 * @description: 文件(FileInfo)实体类
 */
@Getter
@Setter
@ApiModel("文件实体类")
public class FileInfo implements Serializable {
    private static final long serialVersionUID = 694649584012557460L;

    @ApiModelProperty("id")
    private String id;

    @ApiModelProperty("相对路径")
    private String filePath;

    @ApiModelProperty("文件名")
    private String fileName;

    @ApiModelProperty("后缀")
    private String suffix;

    @ApiModelProperty("大小|字节B")
    private Integer fileSize;

    @ApiModelProperty("用途|枚举[FileUseEnum]：COURSE('C', '讲师'), TEACHER('T', '课程')")
    private String fileUse;

    @ApiModelProperty("创建时间")
    private Date createdAt;

    @ApiModelProperty("修改时间")
    private Date updatedAt;

    @ApiModelProperty("已上传分片")
    private Integer shardIndex;

    @ApiModelProperty("分片大小|B")
    private Integer shardSize;

    @ApiModelProperty("分片总数")
    private Integer shardTotal;

    @ApiModelProperty("文件标识")
    private String fileKey;

    @ApiModelProperty("base64")
    private String shard;

    @ApiModelProperty("vod|阿里云vod")
    private String vod;

}