package com.javaboy.vhr.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author: gaoyang
 * @date: 2021-02-03 17:46:47
 * @description: 政治面貌(Politicsstatus)实体类
 */
@Getter
@Setter
@ApiModel("政治面貌实体类")
public class Politicsstatus implements Serializable {
    private static final long serialVersionUID = 988565667254774193L;

    @ApiModelProperty("自增主键")
    private Integer id;

    @ApiModelProperty("政治面貌")
    private String name;

}